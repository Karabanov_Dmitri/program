package Program10;

import java.util.Scanner;

/**
 * Created by karab on 24.01.2017.
 */
public class Program10 {
    public static void main(String[] arg) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число для проверки");
        int i = scanner.nextInt();
        if (String.valueOf(i).equals(new StringBuilder(String.valueOf(i)).reverse().toString())) {
            System.out.println("число палиндром");
        } else {
            System.out.println("число не палиндром");
        }
        scanner.close();
    }
}






package Program12;

import java.util.Scanner;

/**
 * Created by karab on 24.01.2017.
 */
public class Program12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите количество суток");
        int sut = scanner.nextInt();
        int cas = 24 * sut;
        int min;
        int sec;

        min = cas * 60;
        sec = min * 60;
        System.out.println("часов-" + cas + "\nминут-" + min + "\nсекунд-" + sec);
    }
}

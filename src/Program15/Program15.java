package Program15;

/**
 * Created by karab on 24.01.2017.
 */
public class Program15 {
    public static void main(String[] args) {


        int i, j;
        int[][] mas = new int[5][5];

        for (i = 0; i < 5; ++i) {
            for (j = 0; j < 5; ++j) {
                mas[i][j] = (int) (Math.random() * 20 - 10);
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        for (i = 0; i < 5; ++i) {
            for (j = 0; j < 5; ++j) {
                int tmp = mas[i][j];
                mas[i][j] = mas[j][i];
                mas[j][i] = tmp;

                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }
}


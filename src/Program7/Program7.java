package Program7;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by karab on 20.01.2017.
 */
public class Program7 {
    public static void main(String[] args) {
        Random random = new Random();
        int ugad = random.nextInt(20);
        Scanner scanner = new Scanner(System.in);
        int vvod;
        int c = 0;
        do {
            c++;
            vvod = scanner.nextInt();
            if (ugad > vvod) {
                System.out.println("больше");
            }
            if (ugad < vvod) {
                System.out.println("меньше");
            }
        } while (ugad != vvod);
        System.out.println("победа попыток -" + c);
    }
}